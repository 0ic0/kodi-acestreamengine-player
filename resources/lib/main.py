import requests
import json
import xbmcplugin
import xbmcgui
import xbmc
import xbmcaddon
import xbmcvfs
import sys
try:
    from .ace_scraper import AceSearch
except ImportError:
    from ace_scraper import AceSearch


ADDON = xbmcaddon.Addon()
ID = ADDON.getAddonInfo('id')
ACE_SEVER = ADDON.getSettingString('ace_server')
ACE_PORT = ADDON.getSettingString('ace_port')
HANDLE = int(sys.argv[1])
PLAYLIST_TYPE_AS = ADDON.getSettingBool('as_list')
PLAYLIST_FILE = 'special://profile/addon_data/{}/playlist'.format(ID)
PLAYLIST_FILE_MOD = 'special://profile/addon_data/{}/.last_playlist'.format(ID)

if PLAYLIST_TYPE_AS == 1:
    PLAYLIST_URL = 'http://91.92.66.82/trash/ttv-list/as.json'
else:
    PLAYLIST_URL = 'http://91.92.66.82/trash/ttv-list/ace.json'


def check_file_header():
    if ADDON.getSettingBool('cache_clear') == 1:
        log('Clear Cache set. Skip the header check.')
        ADDON.setSettingBool('cache_clear', False)
        playlist_download()
    playlist = xbmcvfs.File(PLAYLIST_FILE_MOD, 'r')
    head = playlist.read()
    headers = {
        'Accept-Encoding': 'gzip',
        'User-Agent': 'Test'
    }
    url = PLAYLIST_URL
    req = requests.Request(method='HEAD', url=url, headers=headers)
    session = requests.Session()
    response = session.send(session.prepare_request(req))
    if head.startswith(response.headers['Last-Modified']):
        log('Playlist not modified on the server. WIll use our copy')
        pass
    else:
        playlist_download()


def playlist_download():
    headers = {
        'Accept-Encoding': 'gzip',
        'User-Agent': 'Test'
    }
    url = PLAYLIST_URL
    req = requests.Request(method='GET', url=url, headers=headers)
    session = requests.Session()
    response = session.send(session.prepare_request(req))
    if response.ok:
        f = xbmcvfs.File(PLAYLIST_FILE, 'w')
        f_mod = xbmcvfs.File(PLAYLIST_FILE_MOD, 'w')
        f_mod.write('{}'.format(response.headers['Last-Modified']))
        length = f.write(response.content)
        log('Playlist Written: {}'.format(length))
    else:
        log('Bad response from server: {}'.format(response.headers))


def add_channel(name, url):
    _xbmc = xbmcgui.ListItem(label=name)
    _xbmc.setInfo('video', {'title': name})
    xbmcplugin.addDirectoryItem(HANDLE, url=url, listitem=_xbmc, isFolder=False)


def load_category(category):
    f = xbmcvfs.File(PLAYLIST_FILE)
    l = json.load(f)
    for i in l['channels']:
        if i['cat'] == category or i['cat'].encode('ascii', 'ignore') == category:
            resource_url = 'http://{}:{}/ace/getstream?id={}&.mp4'.format(ACE_SEVER, ACE_PORT, i['url'])
            add_channel(i['name'], resource_url)


def load_other():
    f = xbmcvfs.File(PLAYLIST_FILE)
    cl = json.load(f)
    def_categories = ['Sport', 'Movies', 'News', 'Music', 'Entertaining', 'Kids', 'Adult', 'Erotic_18_Plus',
                      'Informational', 'Educational']
    _other = {}
    for i in cl['channels']:
        if i['cat'].title() not in def_categories:
            if _other.get(i['cat']) is None:
                log('New category found: {}'.format(i['cat'].encode('ascii', 'ignore')))
                _other[i['cat']] = True
    for u in _other.keys():
        # Encoding to ascii will break the listing for some of the channels
        create_folder(u.encode('ascii', 'ignore'))


def load_search(result):
    for i in result.keys():
        resource_url = 'http://{}:{}/ace/getstream?id={}'.format(ACE_SEVER, ACE_PORT, result[i])
        add_channel(i, resource_url)


def get_action(action):
    if action is None or action == '':
        create_folders()
        # Download the playlist and process it if necessary
        check_file_header()
    if action.startswith('?category='):
        cat = action.split('?category=')[1]
        log('Loading channels of category: {}'.format(cat))
        if cat == 'news':
            cat = 'informational'
        elif cat == 'adult':
            cat = 'erotic_18_plus'
        elif cat == 'other':
            load_other()
        elif cat == 'search':
            search()
        load_category(cat)


def create_folders():
    # News are in informational category
    # Adult - cat: erotic_18_plus
    categories = ['Sport', 'Movies', 'News', 'Music', 'Entertaining', 'Kids', 'Adult', 'Other', 'Search']
    for i in categories:
        cat = xbmcgui.ListItem(i)
        cat.setIsFolder(True)
        res_url = 'plugin://{}?category={}'.format(ID, i.lower())
        xbmcplugin.addDirectoryItem(HANDLE, url=res_url, listitem=cat, isFolder=True)


def create_folder(category):
    name = xbmcgui.ListItem(category.title())
    name.setIsFolder(True)
    folder_url = 'plugin://{}?category={}'.format(ID, category)
    xbmcplugin.addDirectoryItem(HANDLE, url=folder_url, listitem=name, isFolder=True)


def search(name=False):
    a_s = AceSearch()
    dialog = xbmcgui.Dialog()
    nm = dialog.input('Search', type=xbmcgui.INPUT_ALPHANUM)
    log('Search for: {}'.format(nm))
    search_result = a_s.ace_search(nm)
    if len(search_result) > 0:
        load_search(search_result)
    else:
        pass



def log(msg):
    xbmc.log(msg='{addon}: {msg}'.format(addon=ID, msg=msg), level=xbmc.LOGDEBUG)


def run():
    log('Settings: ID - {} Server - {}'.format(ID, ACE_SEVER))
    log('Called with args [{handle} {args}]'.format(handle=HANDLE, args=sys.argv[2]))
    log('PLAYLIST_AS: {}'.format(PLAYLIST_TYPE_AS))
    get_action(sys.argv[2])
    xbmcplugin.endOfDirectory(HANDLE)

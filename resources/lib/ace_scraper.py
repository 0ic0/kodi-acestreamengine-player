import bs4
import requests
try:
    from urllib.parse import quote
except ImportError:
    from urllib import quote


class AceSearch:

    def __init__(self):
        pass

    @staticmethod
    def __search(data):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Upgrade-Insecure-Requests': '1',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache',
            #'Content-Type': 'application/x-www-form-urlencoded'
            }
        """ acestreamsearch API changes """
        #base_url = 'https://acestreamsearch.com/en/'
        #ch_search = 'cn={}'.format(quote(data))
        #base = requests.Request(method='POST', url=base_url, headers=headers, data=ch_search)
        """ They use GET requests now """
        base_url = 'https://acestreamsearch.com/en/'
        ch_search = '?q={}'.format(quote(data))
        base = requests.Request(method='GET', url=base_url+ch_search, headers=headers)
        sess = requests.Session()
        resp = sess.send(sess.prepare_request(base))
        if resp.ok:
            return resp
        else:
            raise requests.exceptions.HTTPError('Can\'t get result from server')

    @staticmethod
    def parse_html(response):
        soup = bs4.BeautifulSoup(response.text, 'html.parser')
        links = []
        names = []
        list = {}
        for i in soup.find_all('a', href=True):
            if i.get('href').startswith('acestream://'):
                names.append(i.text)
                links.append(i.get('href').split('acestream://')[1])
                list[i.text] = i.get('href').split('acestream://')[1]
        return list

    def ace_search(self, data):
        res = self.__search(data)
        return self.parse_html(res)
